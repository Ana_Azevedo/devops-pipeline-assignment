# pull base image.
FROM maven:3-jdk-8

RUN apt-get update

# Installing Git..
RUN apt-get install -y git

RUN git clone https://Ana_Azevedo:gTkLBTdxAQhjjcyTskQ5@bitbucket.org/Ana_Azevedo/devops-pipeline-assignment.git

# Building the application..
WORKDIR /devops-pipeline-assignment

RUN mvn install
